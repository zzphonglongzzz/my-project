#### Hola Houses

---

Capstone Project name:

- English: Website for Renting and Finding Boarding House.
- Vietnamese: Website đăng tin cho thuê và tìm kiếm nhà trọ.
- Abbreviation: HLB-Hola Houses

### Usage

---

To start the root project, simply install node_module `npm i or npm install` and run `npm start`.

`npm run build` was built with build folder

```bash
# Clone this repository
$ git clone https://gitlab.com/LongBody/my-project.git

# Go into the repository
$ cd my-project

# Install dependencies
$ npm install

# Run the app
$ npm start

# open app in browser
$  Local: http://localhost:3000
$  On Your Network: http://192.168.0.102:3000
```

<br />
<p align="center">
<img src="./src/assets/read-me/run-start.png" align="center"
     alt="Start App">
</p>
<br />

```shell
Options: Using `npm run` to run
  -start              Start web application
  -build              Built with build folder
  -checkTs            To check lint types
```

#### Output Structure

---

```shell
src/
├── app
│   ├── components
│   ├── pages
│   │   ├──  authentications
│   │   ├── commmon
│   │   ├── landlord
│   │   ├── user
│   │   └── admin
├── assets
├── helpers
├── hooks
├── locales
│   ├── en
│   └── vi
├── sagas
├── store
│   ├── configureStore
│   └── reducers
├── styles
├── types
├── ultis
└── index
```

#### License

---

This project is licensed under the terms of <a href="https://hoalac.netlify.app/">Hola Houses</a>
